int lastUpdate;
int lastTime = 0;
int seconds = 0;
Tile firstTile;

Building bToTransfert;
Player p;
int[] rgb;
ArrayList<Tile> tilesUnderConstruction;

boolean jobDone = false;
boolean barnBuilt = false;

PGraphics pg;
final int tileSize = 40;
Map map;
Menu menu;

String mouseStatus;

Building thingInConstruction;
ActionButton AB_thingInContruction;
boolean constructionStatus;


void setup () 
{
  p = new Player();
  firstTile = null;
  pg = new PGraphics();
  size (1800, 980);
  map = new Map(width, height-60);
  menu = new Menu();
  menu.addActionButton(new ActionButton("Barn",menu.actionButtons.size()));
  
  mouseStatus = "none";
  thingInConstruction = null;
  AB_thingInContruction = null;
  constructionStatus = false;
}

void update() 
{
  pg.beginDraw();
  pg.loadPixels();  
  
  map.DisplayAll();
  p.drawPlayer();
  
  textSize(15);
   fill(0,0,0);
   text("Timer : " + seconds, width - 150, 15); 
  
  
  Building barn = thingInConstruction;
  if(p.toDo == "Barn")
  {
    barn = thingInConstruction;
    
    if(!p.There(barn.position.x,barn.position.y)) {p.GoThere(barn.position.x,barn.position.y);}
    else 
    {
      EndBuilding();
      barnBuilt = true;
    }
  }else if(p.toDo == "WoodCutter" || p.toDo == "Stone Miner" || p.toDo == "Iron Miner")
  {
    StartBuilding(barn);
  }

   if( millis() - lastTime >= 1000){
    seconds ++;
    for (int i = 0; i < map.buildings.size(); i++) 
    {
      
      Building b = map.buildings.get(i);
      b.GenerateMaterial();
    }
    
    lastTime = millis();
    }

  for (int i = 0; i < map.buildings.size(); i++) 
  {
    Building b = map.buildings.get(i);
    if(b.isFull())
    {
      b.DrawFull();
    }
  }
  
  if(mouseStatus == "building"){
    if(thingInConstruction != null){
      /* Square of building localisation */
      if(mouseX >= 0 && mouseY <= height-60)
      {
        map.MouseOnTile();
        int positionMatrix[] = map.FindTileOnMouse();
        if(positionMatrix != null)
        {               
          constructionStatus = map.setSomeTiles(positionMatrix[0], positionMatrix[1], 4, 4, OnGround.BUILDT, Colors.zone.getColor());
        }
      }
    }
  }
  
  if(mouseStatus == "Transfert")
  {
    p.TransfertToBarn(map.buildings.get(0),bToTransfert);
    switch(bToTransfert.usage)
    {
      case "WoodCutter": if(p.woodTransfered == bToTransfert.maxCapacity)
                           {
                             jobDone = true;
                             p.woodTransfered = 0;
                             p.toDo = "none";
                             mouseStatus = "none";
                           }
      break;
      case "Stone Miner": if(p.stoneTransfered == bToTransfert.maxCapacity)
                           {
                             jobDone = true;
                             p.stoneTransfered = 0;
                             p.toDo = "none";
                             mouseStatus = "none";
                           }
      break;
      case "Iron Miner": if(p.ironTransfered == bToTransfert.maxCapacity)
                           {
                             jobDone = true;
                             p.ironTransfered = 0;
                             p.toDo = "none";
                             mouseStatus = "none";
                           }
      break;
    }
  }
  
  
  menu.drawMenu();  
  
  pg.updatePixels();
  pg.endDraw();
}

void draw () {
  background (255);
  
   
  update();
  pg.image(pg,0,0); 
  
   
}

void mousePressed() 
{
  if(mouseButton == LEFT)
  {
    if(mouseStatus == "building")
    {
      if(constructionStatus == true)
      {
        tilesUnderConstruction = map.getSomeTiles(Colors.zone);
        
        switch(thingInConstruction.usage)
        {
          case "Barn" : rgb = Colors.barn.getColor();
          p.toDo = "Barn";
          break;         
          case "WoodCutter" : rgb = Colors.woodcutter.getColor();
          p.toDo = "WoodCutter";
          break;
          case "Iron Miner" : rgb = Colors.ironminer.getColor();
          p.toDo = "Iron Miner";
          break;
          case "Stone Miner" : rgb = Colors.stoneminer.getColor(); //<>//
          p.toDo = "Stone Miner";
          break;
          default: rgb = null;
        }
          mouseStatus = "none";
          int pos[] = map.FindTileOnMouse();
          thingInConstruction.setPosition(map.matrix[pos[0]][pos[1]].position_x,map.matrix[pos[0]][pos[1]].position_y); 
      }
    }else
    {
      if(menu.getActionButtons() != null)
      {
        Boolean found = false;
        int i = 0;
        AB_thingInContruction = null; //<>//
        while(i < menu.getActionButtons().size() && found != true)
        {
          AB_thingInContruction = menu.getActionButtons().get(i);
          found = AB_thingInContruction.ifClicked();
          i++;        
        }
        if(AB_thingInContruction != null)
        {
          if(found){ 
            thingInConstruction = new Building(AB_thingInContruction.usage, BuildingStatus.BUILDING);
            if(menu.woodAmount >= thingInConstruction.woodCost && menu.stoneAmount >= thingInConstruction.stoneCost && menu.ironAmount >= thingInConstruction.ironCost )
            {
              mouseStatus = "building";
              
            }else mouseStatus = "none";
             
          }
        }
      } //<>//

      if(mouseStatus == "none")
      {
        for (int i = 0; i < map.buildings.size(); i++) 
        {
          Building b = map.buildings.get(i);
          if(b.isClicked() && b.isFull())
          {
            bToTransfert = b;
            mouseStatus = "Transfert";
            p.toDo = "Transfert";
            jobDone = false;
          }
        }
      }
    }
  }else
  {
    if(mouseButton == RIGHT)
    {
      map.GoBackDefault();
      mouseStatus = "none";
    }
  }
}

void EndBuilding()
{
  if(p.toDo != "none") //<>//
  {
    for (int i = 0; i < tilesUnderConstruction.size(); i++) 
    { 
      if(i == 0) {firstTile = tilesUnderConstruction.get(i);}
      Tile t = tilesUnderConstruction.get(i);  
      t.defaultColor = color(rgb[0],rgb[1],rgb[2]);
      t.groundColor = color(rgb[0],rgb[1],rgb[2]);
      t.ground = OnGround.BUILDT;
    }   
    
    map.buildings.add(thingInConstruction);
    constructionStatus = false;
     
    
    switch(thingInConstruction.usage)
    {
      case("Barn"): menu.addActionButton(new ActionButton("WoodCutter",menu.actionButtons.size())); 
                    menu.RemoveMaterial(thingInConstruction.woodCost,thingInConstruction.stoneCost,thingInConstruction.ironCost);
                    p.toDo = "none";
      break;
      case("WoodCutter"): menu.addActionButton(new ActionButton("Stone Miner",menu.actionButtons.size()));  
                          p.toDo = "none";
      break;
      case("Stone Miner"): menu.addActionButton(new ActionButton("Iron Miner",menu.actionButtons.size()));
                           p.toDo = "none";
      break;
    }
  }
  jobDone = false;
}

void StartBuilding(Building barn)
{
    if(map.buildings.size() != 0) barn = map.buildings.get(0);
    if(p.woodTransfered != thingInConstruction.woodCost || p.stoneTransfered != thingInConstruction.stoneCost || p.ironTransfered != thingInConstruction.ironCost ){
      
      p.TransfertMaterial(barn,thingInConstruction);
      jobDone = false;
      if(p.woodTransfered == thingInConstruction.woodCost && p.stoneTransfered == thingInConstruction.stoneCost && p.ironTransfered == thingInConstruction.ironCost) jobDone = true;
    }
    if(jobDone){
      EndBuilding(); 
      p.toDo = "none";
      p.ironTransfered = 0;
      p.stoneTransfered = 0;
      p.woodTransfered = 0;
      jobDone = false;
    }
}
