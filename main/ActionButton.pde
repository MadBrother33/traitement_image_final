class ActionButton
{
  final PVector size = new PVector(40,40);
  PShape square;
  PVector position;
  String usage;
  
  ActionButton(String usage,int nb)
  {
    this.usage = usage;
    position = new PVector();
    square = new PShape();
    
    position.x = 20 + nb*(40+20);
    position.y = height-45;
  }
  
  void drawButton()
  {
    square = createShape(RECT, position.x, position.y, size.x, size.y);
    square.setFill(color(0,0,0));
    shape(square);
    
    textSize(20);
    text(usage.charAt(0), position.x + 15, position.y + 27.5);
  }
  
  Boolean ifClicked()
  {
    if(mouseX >= position.x && mouseX <= position.x+size.x  && mouseY >= position.y && mouseY <= position.y + size.y)
    {
      return true;
    }else
      return false;
  }
}
