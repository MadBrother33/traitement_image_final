class Building
{
   PVector position;
   BuildingStatus status;
   String usage;
   
   int capacity;
   int maxCapacity;
   
   int woodCost;
   int stoneCost;
   int ironCost;
   int genSpeed;
   
   int size = 160;
   
   Building(String usage, BuildingStatus status)
   {
     this.status = status;
     this.usage = usage;
     position = new PVector();
     capacity = 0;
     maxCapacity = 200;
     
     switch(usage)
     {
       case "Barn":
         woodCost = 300;
         stoneCost = 100;
         ironCost = 40;
         genSpeed = 0;
         break;
       case "WoodCutter":
         woodCost = 160;
         stoneCost = 60;
         ironCost = 0;
         genSpeed = 20;
         break;
       case "Stone Miner":
         woodCost = 300;
         stoneCost = 0;
         ironCost = 0;
         genSpeed = 20;
         break;
       case "Iron Miner":
         woodCost = 300;
         stoneCost = 200;
         ironCost = 0;
         genSpeed = 20;
         break;
     }
   }
   
   void GenerateMaterial()
   {
     if(capacity + genSpeed > maxCapacity)
       {
         capacity = maxCapacity;
       }else{ capacity += genSpeed;}
   }
   
   boolean isFull(){
     return(capacity == maxCapacity);
   }
   
   void DrawFull()
   {
     textSize(12);
     fill(0,0,0);
     text("Max capacity reached !", position.x + 20, position.y + 20);     
   }
   
   void setPosition(float x,float y){
     position.x = x;
     position.y = y;
   }
   
   boolean isClicked()
   {
     return (mouseX >= position.x && mouseX <= position.x + size && mouseY >= position.y && mouseY <= position.y + size);
   }
}
