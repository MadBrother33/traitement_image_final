class Map
{
  ArrayList<Building> buildings;
  
 PVector mapSize;
 int[] positionMatrix;
  
  Tile[][] matrix;
  int maxcol;
  int maxrow;
  
  Map(int w, int h)
  {
    maxcol = w/40;
    maxrow = h/40;
    matrix = new Tile[maxcol][maxrow];    
    
    mapSize = new PVector();
    mapSize.x = w;
    mapSize.y = h;
    buildings = new ArrayList<Building>();
    
    positionMatrix = new int[2];
    
    InitializeTiles();
  }
  
  void GoBackDefault()
  {
    for(int i = 0; i < maxcol; i++)
    {
      for(int j = 0; j < maxrow; j++)
      { 
        matrix[i][j].groundColor = matrix[i][j].defaultColor;        
      }
    }
  }
  
  
  void MouseOnTile()
  {
    for(int i = 0; i < maxcol; i++)
    {
      for(int j = 0; j < maxrow; j++)
      { 
        if(mouseX >= matrix[i][j].position_x && mouseX <= matrix[i][j].position_x + matrix[i][j].size && mouseY >= matrix[i][j].position_y && mouseY <= matrix[i][j].position_y + matrix[i][j].size)
        {}else
        {
          matrix[i][j].groundColor = matrix[i][j].defaultColor;
        }
        
      }
    }
  }
  
  
  int[] FindTileOnMouse()
  {
    
    boolean found = false;
    
      int i = 0;
      while(i < maxcol && !found)
      {
        for(int j = 0; j < maxrow; j++)
        { 
          found = matrix[i][j].IfMouseOnIt();
          if(found)
          {
            positionMatrix[0] = i;
            positionMatrix[1] = j;
            return positionMatrix;
          }else
          {
             matrix[i][j].groundColor =  matrix[i][j].defaultColor;
          }
        }
        i++;
      } 
    return null;
  }
  
  void drawMatrix()
  {
    
   
    for(int i = 0; i < mapSize.x; i = i+40)
    {
      
    }
    
    for(int j = 0; j < mapSize.y; j = j+40)
    {
      
    }
  }
  
  private void InitializeTiles()
  {
    /* BORDERS */
    for(int i = 0; i < maxcol; i++)
    {
      for(int j = 0; j < maxrow; j++)
      { 
        matrix[i][j] = new Tile(OnGround.MOUNTAIN, Colors.mountain, i*40, j*40 );
      }
    }
    /* END BORDERS */
    
    
    /* GRASS */
    for(int i = 1; i < maxcol-1; i++)
    {
      for(int j = 1; j < maxrow-1; j++)
      { 
        matrix[i][j] = new Tile(OnGround.GRASS, Colors.grass, i*40, j*40 );
      }
    }
    /* END GRASS */
    
    
    /* LAKE */
    for(int i = maxcol-22; i < maxcol-1; i++)// SAND
    {
      for(int j = maxrow-10; j < maxrow-1; j++)
      { 
        matrix[i][j] = new Tile(OnGround.SAND, Colors.sand, i*40, j*40 );
      }
    }
    
      for(int j = maxrow-10; j < maxrow-4; j++)
      { 
        matrix[maxcol-22][j] = new Tile(OnGround.GRASS, Colors.grass, (maxcol-22)*40, j*40 );
      }
      
      for(int i = maxcol-21; i < maxcol-19; i++)
      for(int j = maxrow-10; j < maxrow-5; j++)
      { 
        matrix[i][j] = new Tile(OnGround.GRASS, Colors.grass, i*40, j*40 );
      }
      for(int i = maxcol-19; i < maxcol-16; i++)
      for(int j = maxrow-10; j < maxrow-7; j++)
      { 
        matrix[i][j] = new Tile(OnGround.GRASS, Colors.grass, i*40, j*40 );
      }
      for(int i = maxcol-16; i < maxcol-13; i++)
      for(int j = maxrow-10; j < maxrow-8; j++)
      { 
        matrix[i][j] = new Tile(OnGround.GRASS, Colors.grass, i*40, j*40 );
      }
      for(int i = maxcol-13; i < maxcol-10; i++)
      for(int j = maxrow-10; j < maxrow-9; j++)
      { 
        matrix[i][j] = new Tile(OnGround.GRASS, Colors.grass, i*40, j*40 );
      }
    
    
    for(int i = maxcol-19; i < maxcol-16; i++)// Grass
    {
      for(int j = maxrow-10; j < maxrow-8; j++)
      { 
        matrix[i][j] = new Tile(OnGround.GRASS, Colors.grass, i*40, j*40 );
      }
    }
    
    
    for(int i = maxcol-6; i < maxcol-1; i++)//WATER
    {
      for(int j = maxrow-7; j < maxrow-1; j++)
      { 
        matrix[i][j] = new Tile(OnGround.WATER, Colors.water, i*40, j*40 );
      }
    }
    
    for(int i = maxcol-10; i < maxcol-6; i++)
    {
      for(int j = maxrow-6; j < maxrow-1; j++)
      { 
        matrix[i][j] = new Tile(OnGround.WATER, Colors.water, i*40, j*40 );
      }
    }
    
    for(int i = maxcol-14; i < maxcol-10; i++)
    {
      for(int j = maxrow-5; j < maxrow-1; j++)
      { 
        matrix[i][j] = new Tile(OnGround.WATER, Colors.water, i*40, j*40 );
      }
    }
    
    for(int i = maxcol-17; i < maxcol-14; i++)
    {
      for(int j = maxrow-4; j < maxrow-1; j++)
      { 
        matrix[i][j] = new Tile(OnGround.WATER, Colors.water, i*40, j*40 );
      }
    }
    
    for(int i = maxcol-19; i < maxcol-17; i++)
    {
      for(int j = maxrow-3; j < maxrow-1; j++)
      { 
        matrix[i][j] = new Tile(OnGround.WATER, Colors.water, i*40, j*40 );
      }
    }
    
    for(int i = maxcol-20; i < maxcol-19; i++)
    {
      for(int j = maxrow-2; j < maxrow-1; j++)
      { 
        matrix[i][j] = new Tile(OnGround.WATER, Colors.water, i*40, j*40 );
      }
    }
    /* END LAKE */
    
    /* FOREST */
    
    for(int i = 1; i < 10; i++)
    {
      for(int j = 1; j < 8; j++)
      { 
        matrix[i][j] = new Tile(OnGround.TREE, Colors.tree, i*40, j*40 );
      }
    }
    
    for(int i = 1; i < 8; i++)
    {
      for(int j = 1; j < 11; j++)
      { 
        matrix[i][j] = new Tile(OnGround.TREE, Colors.tree, i*40, j*40 );
      }
    }
    
    for(int i = 1; i < 5; i++)
    {
      for(int j = 1; j < 15; j++)
      { 
        matrix[i][j] = new Tile(OnGround.TREE, Colors.tree, i*40, j*40 );
      }
    }
    for(int i = 1; i < 13; i++)
    {
      for(int j = 1; j < 5; j++)
      { 
        matrix[i][j] = new Tile(OnGround.TREE, Colors.tree, i*40, j*40 );
      }
    }
    for(int i = 1; i < 15; i++)
    {
      for(int j = 1; j < 3; j++)
      { 
        matrix[i][j] = new Tile(OnGround.TREE, Colors.tree, i*40, j*40 );
      }
    }
    
    /* END FOREST */
    
    /* ROCK */
    
    for(int i = 25; i < 31; i++)
    {
      for(int j = 7; j < 12; j++)
      { 
        matrix[i][j] = new Tile(OnGround.ROCK, Colors.rock, i*40, j*40 );
      }
    }
    
    /* END ROCK */
  }
  
  void DisplayTiles()
  {
    for(int i = 0; i < maxcol; i++)
    {
      for(int j = 0; j < maxrow; j++)
      { 
        matrix[i][j].DisplayTile();
      }
    }
  }
  
  void DisplayAll()
  {
    this.DisplayTiles();
  }
  
  
  int getSize()
  {
    return maxcol*maxrow;
  }
  
  boolean setSomeTiles(int col, int row, int nbTilesWidth,int nbTilesHeight, OnGround ground, int[] rgb)
  {    
    boolean construction_ok = true;
    for(int h = col; h<col+nbTilesWidth; h++)
    {
      for(int k = row; k<row+nbTilesHeight;k++)
      {
        if(h< maxcol && k<maxrow)
        {
          if(!(matrix[h][k].ground == OnGround.BUILDT || matrix[h][k].ground == OnGround.WATER || matrix[h][k].ground == OnGround.TREE || matrix[h][k].ground == OnGround.MOUNTAIN))
          {
            matrix[h][k].groundColor = color (rgb[0],rgb[1],rgb[2]);
          }else
          {
            construction_ok = false;
            matrix[h][k].groundColor = color (255,0,0);
          }
        }
      }
    }
    return construction_ok;
  }
  
  ArrayList<Tile> getSomeTiles(Colors c)
  {
    ArrayList<Tile> tilesList = new ArrayList<Tile>();
    if(mouseX < width)
    {
      int i = 0;
      while(i < maxcol)
      {
        for(int j = 0; j < maxrow; j++)
        { 
          int[] rgb = Colors.zone.getColor();
          if(matrix[i][j].groundColor == color(rgb[0],rgb[1],rgb[2]))
          {
            tilesList.add(matrix[i][j]);
          }
        }
        i++;
      }
    }
    return tilesList;
  }


}
