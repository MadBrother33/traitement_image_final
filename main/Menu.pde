import java.util.Iterator;

class Menu
{
  PVector position;
  ArrayList<ActionButton> actionButtons;
  PShape rect;
  boolean ok;
  
  int woodAmount = 460;
  int stoneAmount = 160;
  int ironAmount = 40;
  
  int nbActionButton;
  
  
  Menu()
  {
    actionButtons = new ArrayList<ActionButton>();
    
    position = new PVector();
    position.x = 0;
    position.y = height-60;
    
    rect = createShape(RECT, position.x,position.y,width,60);
    rect.setFill(color(0,102,153,40));
    
    nbActionButton = actionButtons.size();
  }
  
  void drawMenu()
  {    
    shape(rect);
    if (actionButtons.isEmpty())
    {
      textSize(12);
      text("No buildings available !",position.x + 10, position.y + 20);
      fill(0,102,153);
    }else
    {
      float position_x = position.x + 10;     
      for (int i = 0; i < actionButtons.size(); i++) 
      {
        fill(0,102,153);
        textSize(12);
        text("Buildings :",position.x+ 2 , position.y+12);
        
        ActionButton actionButton = actionButtons.get(i);  
        actionButton.drawButton();
        position_x += 50;
      }
    }
    
    /* Display ressources */
    String text = "Wood :"+ woodAmount;
    text(text,position.x+900,position.y+35);
    text = "Stone :"+stoneAmount;
    text(text,position.x+1200,position.y+35);
    text = "Iron :"+ironAmount;
    text(text,position.x+1500,position.y+35);
    
  }
  
  ArrayList<ActionButton> getActionButtons()
  {
    return actionButtons;
  }
  
  void addActionButton(ActionButton ab){
    actionButtons.add(ab); 
    nbActionButton ++;
  }
  
  void RemoveMaterial(int w, int s, int i){
    woodAmount -= w;
    stoneAmount -= s;
    ironAmount -= i;
  }
  
}
