class Tile
{  
  float position_x;
  float position_y;
  
  color groundColor;
  color defaultColor;
  final float size = 40;
  OnGround ground;
  
  Tile(){}
  
  
  Tile(OnGround ground, Colors c, float x, float y)
  {
    this.ground = ground;
    
    int rgb[] = c.getColor();
    this.groundColor = color(rgb[0],rgb[1],rgb[2]);
    this.defaultColor = groundColor;
    
    this.position_x = x;
    this.position_y = y;
  }
  
  void DisplayTile()
  {
    noStroke();
    PShape square = createShape(RECT,position_x,position_y,size,size);
    square.setFill(groundColor);
    
    shape(square);
  
  }
  
  boolean IfMouseOnIt()
  {
    if(mouseX >= position_x && mouseX <= position_x + size && mouseY >= position_y && mouseY <= position_y + size)
    {
      return true;
    }else
    this.groundColor = defaultColor;
    return false;
  }
  
  float getX(){
    return position_x;
  }
  float getY(){
    return position_y;
  }
  
}
