class Player
{
  PShape circle;
  String toDo;
  PVector position;
  PVector acceleration;
  PVector velocity;
  float movementSpeed = 7;
  
  int capacity = 20;
  boolean haveMaterial =false;
  int woodTransfered = 0;
  int stoneTransfered = 0;
  int ironTransfered = 0;
  
  Player(){
    toDo = "none";
    position = new PVector(500,500);
    
    circle = createShape(ELLIPSE, position.x,position.y,30,30);
    circle.setFill(color(255,0,0,50));
    velocity = new PVector(movementSpeed,movementSpeed);
  }
  
  void drawPlayer(){
    shape(circle);
  }
  
  void GoThere(float destination_x, float destination_y){
    PVector destVector = new PVector(destination_x,destination_y);
    
    PVector direction = PVector.sub(destVector,position);
      
    acceleration = direction;
    
    velocity.add(acceleration);
    velocity.limit(movementSpeed);
    position.add(velocity);

    circle = createShape(ELLIPSE, position.x,position.y,30,30);
    circle.setFill(color(255,0,0,50));
  }
  
  boolean There(float destination_x, float destination_y)
  {
    if(position.x -5 < destination_x && position.x +5 > destination_x && position.y -5 < destination_y && position.y +5 > destination_y ) return true;
    else return false;
  }
  
  void TransfertMaterial(Building barn, Building construction){
     if(woodTransfered != construction.woodCost) TransfertWood(barn, construction);
     else if (stoneTransfered != construction.stoneCost) TransfertStone(barn,construction);
     else if (ironTransfered != construction.ironCost) TransfertIron(barn,construction);
  }
  
  void TransfertWood(Building barn, Building construction)
  {
    if(!There(barn.position.x,barn.position.y) && haveMaterial == false ) p.GoThere(barn.position.x,barn.position.y);
     else 
     {
       if(!haveMaterial)menu.woodAmount -= capacity;
       haveMaterial = true;
       
       if(!There(construction.position.x,construction.position.y)) p.GoThere(construction.position.x,construction.position.y);
       else
       {
         haveMaterial = false;
         woodTransfered += capacity;
       }
     }
  }
  
  void TransfertStone(Building barn, Building construction)
  {
    if(!There(barn.position.x,barn.position.y) && haveMaterial == false ) p.GoThere(barn.position.x,barn.position.y);
     else 
     {
       if(!haveMaterial) menu.stoneAmount -= capacity;
       haveMaterial = true;
       if(!There(construction.position.x,construction.position.y)) p.GoThere(construction.position.x,construction.position.y);
       else
       {
         haveMaterial = false;
         stoneTransfered += capacity;
       }
     }
  }
  
  void TransfertIron(Building barn, Building construction)
  {
    if(!There(barn.position.x,barn.position.y) && haveMaterial == false ) p.GoThere(barn.position.x,barn.position.y);
     else 
     {
       if(!haveMaterial)menu.ironAmount -= capacity;
       haveMaterial = true;
       
       if(!There(construction.position.x,construction.position.y)) p.GoThere(construction.position.x,construction.position.y);
       else
       {
         haveMaterial = false;
         ironTransfered += capacity;
       }
     }
  }
  
  void TransfertToBarn(Building barn, Building b)
  {
    if(!p.There(b.position.x,b.position.y) && haveMaterial == false) GoThere(b.position.x,b.position.y);
    else
    {
      if(!haveMaterial){ b.capacity -= capacity;}
       haveMaterial = true;
       
       if(!There(barn.position.x,barn.position.y)) p.GoThere(barn.position.x,barn.position.y);
       else
       {
         haveMaterial = false;
         
         switch(b.usage)
          {
            case "WoodCutter": menu.woodAmount += capacity;
                               woodTransfered += capacity;
            break;
            case "Stone Miner": menu.stoneAmount += capacity;
                                stoneTransfered += capacity;
            break;        
            case "Iron Miner": menu.ironAmount += capacity;
                               ironTransfered += capacity;
            break;
          }
       }
    }
  }
}
