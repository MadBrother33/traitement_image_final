enum Colors
{
  grass (0,153,10),//#00990A
  tree (0,97,21),//#006115
  woodcutter (160,85,0),//#000000
  stoneminer(255,255,255),
  ironminer(60,60,60),
  rode (114,64,13),//#72400D
  mountain (89,89,89),//#595959
  sand (242,242,0),//#F2F200
  rock (164,164,164),//#A4A5A4
  water (7,0,232),//#0700E8
  zone(152,255,145),
  barn(255,255,155);
  
  
  private int rgb[];
  private Colors(int r, int g, int b){
    rgb = new int[3];
    rgb[0] = r;
    rgb[1] = g;
    rgb[2] = b;
    
  }
  int[] getColor()
  {
    return rgb;
  }
}
